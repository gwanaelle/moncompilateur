# CERIcompiler

A simple compiler.
From : Pascal-like imperative LL(k) langage
To : 64 bit 80x86 assembly langage (AT&T)

**Download the repository :**

> git clone git@framagit.org:jourlin/cericompiler.git

**Build the compiler and test it :**

> make test

**Have a look at the output :**

> gedit test.s

**Debug the executable :**

> ddd ./test

**Commit the new version :**

> git commit -a -m "What's new..."

**Send to your framagit :**

> git push -u origin master

**Get from your framagit :**

> git pull -u origin master

**This version Can handle :**

//https://framagit.org/gwanaelle/moncompilateur

// Program := [DeclarationPart] StatementPart | TypeDefinitionPart
// StatementPart := Statement {";" Statement} "."
// AssignementStatement := Identifier ":=" Expression

//TP4
// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"

//TP3
// IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
// WhileStatement := "WHILE" Expression "DO" Statement
// ForStatement := "For" ID ":=" Expression ("TO"|"DOWNTO") Expression "DO" Statement
// BlockStatement := "BEGIN" Statement { ";" Statement } "END"

//TP5
// DisplayStatement := "DISPLAY" <expression>
// Statement := AssignementStatement | IfStatement | WhileStatement | 
//					ForStatement | BlockStatement | DisplayStatement

//TP6
// Type := "INTEGER" | "BOOLEAN"
// VarDeclaration := Ident {"," Ident} ":" Type
// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."

//TP7
//CharConst

//TP8
//RepeatStatement := "REPEAT" Statement { ";" Statement } "UNTIL" Expression

//Empty :=
//Constant := Number | Identifier | CharConst
//CaseLabelList := Constant {,Constant}
//CaseListElement := CaseLabelList : Statement | Empty
//CaseStatement := "CASE" Expression "OF" CaseListElement {; CaseListElement} END

//RecordSection := Identifier {, Identifier} : type | Empty
//FixedPart := RecordSection {; RecordSection}
//Variant := CaseLabelList : FieldList | Empty
//VariantPart := "CASE" Identifier "OF" Variant {; Variant}
//FieldList := FixedPart | FixedPart; VariantPart | VariantPart
//RecordType := "RECORD" FieldList "END"
//TypeDefinition := Identifier "=" RecordType
//TypeDefinitionPart := Empty | "TYPE" TypeDefinition {; TypeDefinition}

