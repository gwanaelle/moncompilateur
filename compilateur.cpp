//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <map>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPE  {INTEGER, BOOLEAN, DOUBLE, CHAR}; 	//TP4/TP7

TOKEN current;						// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

	
map<string, enum TYPE> DeclaredVariables;
unsigned long long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}


// Program := [DeclarationPart] StatementPart | TypeDefinitionPart
// StatementPart := Statement {";" Statement} "."
// AssignementStatement := Identifier ":=" Expression

//TP4
// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"

//TP3
// IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
// WhileStatement := "WHILE" Expression "DO" Statement
// ForStatement := "For" ID ":=" Expression ("TO"|"DOWNTO") Expression "DO" Statement
// BlockStatement := "BEGIN" Statement { ";" Statement } "END"

//TP5
// DisplayStatement := "DISPLAY" <expression>
// Statement := AssignementStatement | IfStatement | WhileStatement | 
//					ForStatement | BlockStatement | DisplayStatement

//TP6
// Type := "INTEGER" | "BOOLEAN"
// VarDeclaration := Ident {"," Ident} ":" Type
// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."

//TP7
//CharConst

//TP8
//RepeatStatement := "REPEAT" Statement { ";" Statement } "UNTIL" Expression

//Empty :=
//Constant := Number | Identifier | CharConst
//CaseLabelList := Constant {,Constant}
//CaseListElement := CaseLabelList : Statement | Empty
//CaseStatement := "CASE" Expression "OF" CaseListElement {; CaseListElement} END

//RecordSection := Identifier {, Identifier} : type | Empty
//FixedPart := RecordSection {; RecordSection}
//Variant := CaseLabelList : FieldList | Empty
//VariantPart := "CASE" Identifier "OF" Variant {; Variant}
//FieldList := FixedPart | FixedPart; VariantPart | VariantPart
//RecordType := "RECORD" FieldList "END"
//TypeDefinition := Identifier "=" RecordType
//TypeDefinitionPart := Empty | "TYPE" TypeDefinition {; TypeDefinition}

//----------------------------------------------------------------------
//------------------------------- TP4/7 ----------------------------------
		
enum TYPE Identifier(void){
	enum TYPE t;
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	t=DeclaredVariables[lexer->YYText()];
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return t;
}

enum TYPE Number(void){
	double f;
	bool decimal=false;
	unsigned int *i;
	enum TYPE t;
	string	n=lexer->YYText();
	if(n.find(".")!=string::npos){
		f=atof(lexer->YYText());
		i=(unsigned int *) &f; 
		cout <<"\tsubq $8,%rsp"<<endl;
		cout <<"\tmovl	$"<<*i<<", (%rsp)"<<endl;
		cout <<"\tmovl	$"<<*(i+1)<<", 4(%rsp)"<<endl;
		t=DOUBLE;
	}
	else{
		cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
		t=INTEGER;
	}
	current=(TOKEN) lexer->yylex();
	return t;
}

enum TYPE CharConst(void){
	cout<<"\tmovq $0, %rax"<<endl;
	cout<<"\tmovb $"<<lexer->YYText()<<",%al"<<endl;
	cout<<"\tpush %rax\t# push a 64-bit version of "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return CHAR;
}


enum TYPE Expression(void);			// Called by Term() and calls Term()

enum TYPE Factor(void){
	enum TYPE t;
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		t=Expression();
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else if (current==NUMBER)
		t=Number();
	else if(current==ID)
		t=Identifier();
	else if (current==CHARCONST)
		t=CharConst();
	else
					Error("'(' ou chiffre ou lettre attendue");
	return t;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
enum TYPE Term(void){
	OPMUL mulop;
	enum TYPE t1, t2;
	t1=Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		t2=Factor();
		if(t1!=t2) Error("les appels n'ont pas le même type");
		switch(mulop){
			case AND:
				if(t1==BOOLEAN){
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t# AND"<<endl;	// store result
				}
				else
					Error("le type de l'opérateur AND doit être boolean");
				break;			
				
			case MUL:
				if(t1==INTEGER){
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t# MUL"<<endl;	// store result
				}
				else if(t1==DOUBLE){
					cout<<"\tfldl	8(%rsp)"<<endl;
					cout<<"\tfldl	(%rsp)"<<endl;
					cout<<"\tfaddp	%st(0),%st(1)"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp"<<endl; 
				}
				else
					Error("le type de l'opérateur MUL doit être integer ou double");
				break;			
				
			case DIV:
				if(t1==INTEGER){
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
					cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
					cout << "\tpush %rax\t# DIV"<<endl;		// store result
				}
				else if(t1==DOUBLE){
					cout<<"\tfldl	(%rsp)"<<endl;
					cout<<"\tfldl	8(%rsp)"<<endl;
					cout<<"\tfaddp	%st(0),%st(1)"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp"<<endl; 
				}
				else
					Error("le type de l'opérateur DIV doit être integer ou double");
				break;
				
			case MOD:
				if(t1==INTEGER){
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
					cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
					cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				}
				else
					Error("le type de l'opérateur MOD doit être integer");
				break;			
				
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return t1;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
enum TYPE SimpleExpression(void){
	OPADD adop;
	enum TYPE t1,t2;
	t1=Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		t2=Term();
		if(t1!=t2) Error("les appels n'ont pas le même type");
		switch(adop){
			case OR:
				if(t1!=BOOLEAN)
					Error("le type de l'opérateur OR doit être boolean");
				cout << "\tpop %rbx"<<endl;	// get first operand
				cout << "\tpop %rax"<<endl;	// get second operand
				cout << "\torq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				cout << "\tpush %rax"<<endl;
				break;			
			case ADD:
				if(t1==INTEGER){
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
					cout << "\tpush %rax"<<endl;
				}
				else if(t1==DOUBLE){
					cout<<"\tfldl	8(%rsp)"<<endl;
					cout<<"\tfldl	(%rsp)"<<endl;
					cout<<"\tfaddp	%st(0),%st(1)"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp"<<endl; 
				}
				else
					Error("le type de l'opérateur ADD doit être integer ou double");
				break;			
			case SUB:	
				if(t1==INTEGER){
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
					cout << "\tpush %rax"<<endl;
				}
				else if(t1==DOUBLE){
					cout<<"\tfldl	(%rsp)"<<endl;
					cout<<"\tfldl	8(%rsp)"<<endl;
					cout<<"\tfaddp	%st(0),%st(1)"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp"<<endl; 
				}
				else
					Error("le type de l'opérateur SUB doit être integer ou double");
				break;			
			default:
				Error("opérateur additif inconnu");
		}
	}
	return t1;

}

//------------------------------- TP6 ----------------------------------

// Type := "INTEGER" | "BOOLEAN"
enum TYPE Type(void){
	if(strcmp(lexer->YYText(),"INTEGER")==0){
		current=(TOKEN) lexer->yylex();
		return INTEGER;
	}
	else if(strcmp(lexer->YYText(),"BOOLEAN")==0){
		current=(TOKEN) lexer->yylex();
		return BOOLEAN;
	}
	else if(strcmp(lexer->YYText(),"DOUBLE")==0){
		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}
	else if(strcmp(lexer->YYText(),"CHAR")==0){
		current=(TOKEN) lexer->yylex();
		return CHAR;
	}
	else
		Error("type attendu");
	
}

// VarDeclaration := Ident {"," Ident} ":" Type
void VarDeclaration(void){
	set<string> Ident;
	enum TYPE t;
	if(current!=ID)
		Error("Un identificateur était attendu");
	Ident.insert(lexer->YYText());
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){		//","
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		Ident.insert(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	if(current!=COLON)
		Error("caractère ':' attendu");
	current=(TOKEN) lexer->yylex();
	t=Type();
	for(set<string>::iterator i=Ident.begin(); i!=Ident.end(); ++i){
		if(t==DOUBLE)
			cout << *i << ":\t.double 0.0"<<endl;
		else if(t==CHAR)
			cout << *i << ":\t.byte 0"<<endl;
		else if(t==INTEGER)
			cout << *i << ":\t.quad 0"<<endl;
		else if(t==BOOLEAN){}
		else
			Error("type attendu");
        DeclaredVariables[*i]=t;
	}
}

// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
void VarDeclarationPart(void){
	current=(TOKEN) lexer->yylex();
	VarDeclaration();
	while(current==SEMICOLON){		//";"
		current=(TOKEN) lexer->yylex();
		VarDeclaration();
	}
	if(current!=DOT)
		Error(" caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
enum TYPE Expression(void){
	OPREL oprel;
	enum TYPE t1,t2;
	t1=SimpleExpression();
	if(current==RELOP){
		oprel=RelationalOperator();
		t2=SimpleExpression();
		if(t1!=t2) Error("les appels n'ont pas le même type");
		if(t1==DOUBLE){
			cout << "\tpop %rax"<<endl;
			cout << "\tpop %rbx"<<endl;
			cout << "\tcmpq %rax, %rbx"<<endl;
		}
		else{
			cout<<"\tfldl	(%rsp)"<<endl;
			cout<<"\tfldl	8(%rsp)"<<endl;
			cout<<"\t addq $16, %rsp"<<endl;
			cout<<"\tfcomip %st(1)"<<endl;
			cout<<"\tfaddp %st(1)"<<endl;
		}	
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;
		return BOOLEAN;
	}
	return t1;
}

// AssignementStatement := Identifier ":=" Expression
void AssignementStatement(void){
	string variable;
	enum TYPE t1,t2;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	t1=DeclaredVariables[variable];
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	t2=Expression();
	if(t1!=t2){
		cerr<<"Type variable "<<t1<<endl;
		cerr<<"Type Expression "<<t2<<endl;
		Error("les appels ont des types incompatibles");
	}
	if(t1==CHAR){
		cout << "\tpop %rax"<<endl;
		cout << "\tmovb %al,"<<variable<<endl;
	}
	else
		cout << "\tpop "<<variable<<endl;
}

//------------------------------- TP3 ----------------------------------

void Statement(void); //appelée par StatementPart, IfStatement ... et les appelle 

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

//IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
void IfStatement(void){
	unsigned long long tag=TagNumber++;
	cout<<"IF"<<tag<<":"<<endl;
	current=(TOKEN) lexer->yylex();
	if(Expression()!=BOOLEAN)
		Error("Expression doit être de type BOOLEAN");
	cout<<"\tpop %rax"<<endl;
	cout<<"\tcmp $0, %rax"<<endl;
	cout<<"\tje ELSE"<<tag<<endl;	//si c'est faux on passe au ELSE sans faire le THEN
	cout<<"THEN"<<tag<<":"<<endl;
	if(strcmp(lexer->YYText(),"THEN")!=0)
		Error("caractère 'THEN' attendu");
	current=(TOKEN) lexer->yylex();
	Statement();
	cout<<"\tjmp FinIf"<<tag<<endl;	//on va à la fin sans faire le ELSE
	cout<<"ELSE"<<tag<<":"<<endl;
	if(strcmp(lexer->YYText(),"ELSE")!=0)
		Error("caractère 'ELSE' attendu");
	current=(TOKEN) lexer->yylex();
	Statement();
	cout<<"FinIf"<<tag<<":"<<endl;
}

//WhileStatement := "WHILE" Expression "DO" Statement
void WhileStatement(void){
	unsigned long long tag=TagNumber++;
	cout<<"WHILE"<<tag<<":"<<endl;
	current=(TOKEN) lexer->yylex();
	if(Expression()!=BOOLEAN)
		Error("Expression doit être de type BOOLEAN");
	cout<<"\tpop %rax"<<endl;
	cout<<"\tcmp $0, %rax"<<endl;
	cout<<"\tje FinWhile"<<tag<<endl;	//si c'est faux on passe à la fin sans faire le DO
	cout<<"DO"<<tag<<":"<<endl;
	if(strcmp(lexer->YYText(),"DO")!=0)
		Error("caractère 'DO' attendu");
	current=(TOKEN) lexer->yylex();
	Statement();
	cout<<"\tjmp WHILE"<<tag<<endl;		//on retourne au début de la boucle
	cout<<"FinWhile"<<tag<<":"<<endl;
}

// ForStatement := "For" ID ":=" Expression ("TO"|"DOWNTO") Expression "DO" Statement
void ForStatement(void){
	unsigned long long tag=TagNumber++;
	cout<<"FOR"<<tag<<":"<<endl;
	current=(TOKEN) lexer->yylex();
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	if(Expression()!=INTEGER)
		Error("Expression doit être de type INTEGER");
	cout<<"\tpop %rcx"<<endl;
	
	if(strcmp(lexer->YYText(),"TO")==0){ //si on a le caractère TO
		cout<<"TO"<<tag<<":"<<endl;
		current=(TOKEN) lexer->yylex();
		if(Expression()!=INTEGER)
			Error("Expression doit être de type INTEGER");
		cout<<"\tpop %rdx"<<endl;
		cout<<"\tcmpq %rdx, %rcx"<<endl;
		cout<<"\tja FinFor"<<tag<<"\t# If %rcx > %rdx jump to FinFor"<<endl; //fin de la boucle
		cout<<"DO"<<tag<<":"<<endl;
		if(strcmp(lexer->YYText(),"DO")!=0)
			Error("caractère 'DO' attendu");
		current=(TOKEN) lexer->yylex();
		Statement();
		cout << "\taddq	$1, %rcx"<<endl; //on incrémente l'id
		cout<<"\tjmp TO"<<tag<<endl;	//on retourne au début de la boucle
	}
	else if (strcmp(lexer->YYText(),"DOWNTO")==0){ //si on a le caractère DOWNTO
		cout<<"DOWNTO"<<tag<<":"<<endl;
		current=(TOKEN) lexer->yylex();
		Expression();
		cout<<"\tpop %rdx"<<endl;
		cout<<"\tcmpq %rdx, %rcx"<<endl;
		cout<<"\tjb FinFor"<<tag<<"\t# If %rcx < %rdx jump to FinFor"<<endl; //fin de la boucle
		cout<<"DO"<<tag<<":"<<endl;
		if(strcmp(lexer->YYText(),"DO")!=0)
			Error("caractère 'DO' attendu");
		current=(TOKEN) lexer->yylex();
		Statement();
		cout << "\tsubq	$1, %rcx"<<endl;	//on décremente l'id
		cout<<"\tjmp DOWNTO"<<tag<<endl;	//on retourne au début de la boucle
	}
	else
		Error("caractère 'TO' ou 'DOWNTO' attendu");
	cout<<"FinFor"<<tag<<":"<<endl;
}

//BlockStatement := "BEGIN" Statement { ";" Statement } "END"
void BlockStatement(void){
	unsigned long long tag=TagNumber++;
	cout<<"BEGIN"<<tag<<":"<<endl;
	current=(TOKEN) lexer->yylex();
	Statement();
	while (current==SEMICOLON){			//tant qu'on à un ';'
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	cout<<"END"<<tag<<":"<<endl;
	if(strcmp(lexer->YYText(),"END")!=0)
		Error("caractère 'END' attendu");
	current=(TOKEN) lexer->yylex();
}

//------------------------------- TP5 ----------------------------------

//DisplayStatement := "DISPLAY" <expression>
void DisplayStatement(void){
	unsigned long long tag=TagNumber++;
	enum TYPE t;
	cout<<"DISPLAY"<<tag<<":"<<endl;
	current=(TOKEN) lexer->yylex();
	t=Expression();
	if(t==INTEGER){
		cout<<"\tpop %rdx"<<endl;
		cout<<"\tmovq $FormatString1, %rsi\t# \"%llu\\n\""<<endl;
		cout<<"\tmovl $0, %eax"<<endl;
		cout<<"\tcall printf@PLT"<<endl;
	}
	else if (t==BOOLEAN){
			cout << "\tpop %rdx"<<endl;
			cout << "\tcmpq $0, %rdx"<<endl;
			cout << "\tje False"<<tag<<endl;
			cout << "\tmovq $TrueString, %rdi"<<endl;
			cout << "\tjmp FinDisplay"<<tag<<endl;
			cout << "False"<<tag<<":"<<endl;
			cout << "\tmovq $FalseString, %rdi"<<endl;
			cout << "FinDisplay"<<tag<<":"<<endl;
			cout << "\tcall	puts@PLT"<<endl;
	}
	else if (t==DOUBLE){
			cout << "\tmovsd	(%rsp), %xmm0"<<endl;
			cout << "\tsubq	$16, %rsp"<<endl;
			cout << "\tmovsd %xmm0, 8(%rsp)"<<endl;
			cout << "\tmovq $FormatString2, %rdi"<<endl;
			cout << "\tmovq	$1, %rax"<<endl;
			cout << "\tcall	printf"<<endl;
			cout << "nop"<<endl;
			cout << "\taddq $24, %rsp"<<endl;
	}
	else if (t==CHAR){
			cout<<"\tpop %rsi"<<endl;
			cout << "\tmovq $FormatString3, %rdi"<<endl;
			cout << "\tmovl	$0, %eax"<<endl;
			cout << "\tcall	printf@PLT"<<endl;
	}
	else
			Error("mauvais type de Display");
}



//------------------------------- TP8 ----------------------------------

//RepeatStatement := "REPEAT" Statement { ";" Statement } "UNTIL" Expression
void RepeatStatement(void){
	unsigned long long tag=TagNumber++;
	cout<<"REPEAT"<<tag<<":"<<endl;
	current=(TOKEN) lexer->yylex();
	Statement();
	while (current==SEMICOLON){			//tant qu'on à un ';'
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	cout<<"UNTIL"<<tag<<":"<<endl;
	if(strcmp(lexer->YYText(),"UNTIL")!=0)
		Error("caractère 'UNTIL' attendu");
	current=(TOKEN) lexer->yylex();
	if(Expression()!=BOOLEAN)
		Error("Expression doit être de type BOOLEAN");
	cout<<"\tpop %rax"<<endl;
	cout<<"\tcmp $0, %rax"<<endl; //si c'est faux
	cout<<"\tje FinRepeat"<<tag<<endl;
	cout<<"\tjmp REPEAT"<<tag<<endl;
	cout<<"FinRepeat"<<tag<<":"<<endl;
}

//Empty :=
void Empty(void){
	cout<<"\t# Empty"<<endl;}

//Constant
enum TYPE Constant(void){
	enum TYPE t;
	if(current==NUMBER) 
		t=Number();
	else if(current==ID)
		t=Identifier();
	else
		Error("mauvais type de constant");
	cout<<"\tpop %rdx"<<endl; //constante dans %rdx
	cout<<"\tcmp %rdx, %rcx"<<endl;

	return t;
}
//CaseLabelList := Constant {,Constant}
void CaseLabelList(void){
	unsigned long long tag=TagNumber;
	tag=tag-2;
	Constant();
	while (current==COMMA){	//tant qu'on à un ','
		cout<<"\tje Instruction"<<tag<<"\t# If equal jump to instruction"<<endl; //si la constante precedente est égale 
																				//alors on ne regarde pas les suivantes
		current=(TOKEN) lexer->yylex();
		Constant();
	}
}

//CaseListElement := CaseLabelList : Statement | Empty
void CaseListElement(void){
	unsigned long long tag=TagNumber++;
	CaseLabelList();
	cout<<"\tjne CASE"<<tag<<"\t# If not equal jump to next case"<<endl; //on ne fait pas l'instruction
	if(current!=COLON)
		Error("caractère ':' attendu");
	current=(TOKEN) lexer->yylex();
	tag--;
	cout<<"Instruction"<<tag<<":"; //aller ici si la premiere constante est égal
	if(current==SEMICOLON || strcmp(lexer->YYText(),"END")==0)
		Empty();
	else{
		cout<<endl;
		Statement();
	}
}

//CaseStatement := CASE Expression OF CasListElement {;CaseListElement} END
void CaseStatement(void){
	unsigned long long tag=TagNumber++;
	enum TYPE t;
	cout<<"CASE :"<<endl; //etiquette de depart
	current=(TOKEN) lexer->yylex();	
	if(Expression()!=INTEGER)
		Error("Expression doit être de type INTEGER");
	cout<<"\tpop %rcx"<<endl; //resultat de expression dans %rcx
	if(strcmp(lexer->YYText(),"OF")!=0)
		Error("caractère 'OF' attendu");
	current=(TOKEN) lexer->yylex();
	cout<<"CASE"<<tag<<":"<<endl;
	CaseListElement();
	tag++;
	cout<<"\tjmp END"<<endl; //si instruction faite alors on va à la fin
	while (current==SEMICOLON){			//tant qu'on à un ';'
		current=(TOKEN) lexer->yylex();
		cout<<"CASE"<<tag<<":"<<endl;
		CaseListElement();
		tag++;
		cout<<"\tjmp END"<<endl;
	}
	cout<<"CASE"<<tag<<":"<<endl;//si pas de prochain case
	cout<<"END :"<<endl;
	if(strcmp(lexer->YYText(),"END")!=0)
		Error("caractère 'END' attendu");
	current=(TOKEN) lexer->yylex();
}



// Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement | DisplayStatement
void Statement(void){
	if(current==ID)
		AssignementStatement();
	else if(strcmp(lexer->YYText(),"IF")==0)
		IfStatement();
	else if(strcmp(lexer->YYText(),"WHILE")==0)
		WhileStatement();
	else if(strcmp(lexer->YYText(),"FOR")==0)
		ForStatement();
	else if(strcmp(lexer->YYText(),"BEGIN")==0)
		BlockStatement();
	else if(strcmp(lexer->YYText(),"DISPLAY")==0)
		DisplayStatement();	
	else if(strcmp(lexer->YYText(),"CASE")==0)
		CaseStatement();	
	else 
		Error("instruction attendu");
}

//RecordSection := Identifier {, Identifier} : type | Empty
void RecordSection(void){
	set<string> Ident;
	enum TYPE t;
	if(current!=ID)
		Error("Un identificateur est attendu");
	Ident.insert(lexer->YYText());
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){		//","
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur est attendu");
		Ident.insert(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	if(current!=COLON)
		Error("caractère ':' attendu");
	current=(TOKEN) lexer->yylex();
	if(current==SEMICOLON || strcmp(lexer->YYText(),"END")==0)
		Empty();
	else{
		t=Type();
		for(set<string>::iterator i=Ident.begin(); i!=Ident.end(); ++i){
			if(t==DOUBLE)
				cout << *i << ":\t.double 0.0"<<endl;
			else if(t==CHAR)
				cout << *i << ":\t.byte 0"<<endl;
			else if(t==INTEGER)
				cout << *i << ":\t.quad 0"<<endl;
			else if(t==BOOLEAN){}
			else
				Error("type attendu");
			DeclaredVariables[*i]=t;
		}
	}
}

//FixedPart := RecordSection {; RecordSection}
void FixedPart(void){
	RecordSection();
	while (current==SEMICOLON){			//tant qu'on à un ';'
		current=(TOKEN) lexer->yylex();
		RecordSection();
	}
}

void FieldList(void); //appelée par Variant

//Variant := CaseLabelList : "(" FieldList ")" | Empty
void Variant(void){
	unsigned long long tag=TagNumber;
	tag=tag-2;
	CaseLabelList();
	cout<<"\tjne CASE"<<tag<<"\t# If not equal jump to next case"<<endl; //on ne fait pas l'instruction
	if(current!=COLON)
		Error("caractère ':' attendu");
	current=(TOKEN) lexer->yylex();
	cout<<"Instruction"<<tag<<":"; //aller ici si la premiere constante est égal
	
	if(current==SEMICOLON || strcmp(lexer->YYText(),"END")==0)
		Empty();
	else{
		if(current!=RPARENT)
			Error("caractère '(' attendu");
		current=(TOKEN) lexer->yylex();
		cout<<endl;
		FieldList();
		if(current!=LPARENT)
			Error("caractère ')' attendu");
		current=(TOKEN) lexer->yylex();
	}
}

//VariantPart := "CASE" Identifier "OF" Variant {; Variant}
void VariantPart(void){
	unsigned long long tag=TagNumber;
	tag=tag-3;
	enum TYPE t;
	cout<<"CASE :"<<endl; //etiquette de depart
	current=(TOKEN) lexer->yylex();	
	if(current!=ID)
		Error("Un identificateur est attendu");
	cout<<"\tpop %rcx"<<endl; //ident dans %rcx
	current=(TOKEN) lexer->yylex();	
	if(strcmp(lexer->YYText(),"OF")!=0)
		Error("caractère 'OF' attendu");
	current=(TOKEN) lexer->yylex();
	cout<<"CASE"<<tag<<":"<<endl;
	Variant();
	tag++;
	cout<<"\tjmp FinCase"<<endl; //si instruction faite alors on va à la fin
	while (current==SEMICOLON){			//tant qu'on à un ';'
		current=(TOKEN) lexer->yylex();
		cout<<"CASE"<<tag<<":"<<endl;
		Variant();
		tag++;
		cout<<"\tjmp FinCase"<<endl;
	}
	cout<<"CASE"<<tag<<":"<<endl;//si pas de prochain case
	cout<<"FinCase :"<<endl;
}

//FieldList := FixedPart | FixedPart; VariantPart | VariantPart
void FieldList(void){
	if(strcmp(lexer->YYText(),"CASE")==0)
		VariantPart();
	else {
		FixedPart();
		if(current==SEMICOLON)
			VariantPart();
	}
}

//RecordType := "RECORD" FieldList "END"
void RecordType(void){
	unsigned long long tag=TagNumber++;
	tag--;
	cout<<"RECORD"<<tag<<":"<<endl;
	current=(TOKEN) lexer->yylex();	
	FieldList();
	if(strcmp(lexer->YYText(),"END")!=0)
		Error("caractère 'END' attendu");
	current=(TOKEN) lexer->yylex();
}

//TypeDefinition := Identifier "=" RecordType
void TypeDefinition(void){
	set<string> Ident;
	enum TYPE t;
	if(current!=ID)
		Error("Un identificateur est attendu");
	Ident.insert(lexer->YYText());
	current=(TOKEN) lexer->yylex();	
	if(current!=EQUAL)
		Error("caractère '=' attendu");
	current=(TOKEN) lexer->yylex();	
	if(strcmp(lexer->YYText(),"RECORD")==0)
		RecordType();
	else 
		Error("caractère 'RECORD' attendu");
}

//TypeDefinitionPart := "TYPE" TypeDefinition {; TypeDefinition}
void TypeDefinitionPart(void){
	unsigned long long tag=TagNumber++;
	cout<<"TYPE"<<tag<<":"<<endl;
	current=(TOKEN) lexer->yylex();	
	TypeDefinition();
	while (current==SEMICOLON){			//tant qu'on à un ';'
		current=(TOKEN) lexer->yylex();
		TypeDefinition();
	}
}

// Program := [DeclarationPart] StatementPart | TypeDefinitionPart
void Program(void){
	if(strcmp(lexer->YYText(),"VAR")==0)
		VarDeclarationPart();
	if(strcmp(lexer->YYText(),"TYPE")==0)
		TypeDefinitionPart();
	else 
		StatementPart();	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	cout << ".data"<<endl;
	cout << "FormatString1:\t.string \"%llu\\n\"\t# used by printf to display 64-bit unsigned integers"<<endl; 
	cout << "FormatString2:\t.string \"%lf\"\t# used by printf to display 64-bit floating point numbers"<<endl; 
	cout << "FormatString3:\t.string \"%c\"\t# used by printf to display a 8-bit single character"<<endl; 
	cout << "TrueString:\t.string \"TRUE\"\t# used by printf to display the boolean value TRUE"<<endl; 
	cout << "FalseString:\t.string \"FALSE\"\t# used by printf to display the boolean value FALSE"<<endl; 

	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}

